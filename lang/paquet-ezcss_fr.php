<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'ezcss_description' => 'Permet de constuire un site complexe pour tous les navigateurs récents. Contrairement aux autres framework qui utilisent une grille, celui-ci se sert de blocs que l\'on peut imbriquer ou non. Un framework CSS est utilisé essentiellement pour permettre à un site d\'apparaître de la même facon quelque soit le navigateur utilisé par le visiteur. EZ-CSS (qui se prononce easy-css) rend cette fonctionnalité très simple à mettre en place même pour des sites très complexes.',
	'ezcss_nom' => 'EZ-CSS',
	'ezcss_slogan' => 'Framework CSS simple et efficace',
);
